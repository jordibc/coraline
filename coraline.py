#!/usr/bin/env python3

"""
Measure the areas of corals from pictures.
"""

import sys
import os
import signal
import warnings

from PIL import Image, ExifTags

from PyQt6.QtCore import Qt, QPoint
from PyQt6.QtGui import QAction, QPixmap, QPainter, QPen, QIcon, QBrush, QColor
from PyQt6.QtWidgets import (
    QApplication, QMainWindow, QWidget, QMenu, QStyle,
    QMessageBox, QInputDialog, QFileDialog)


class Canvas(QWidget):
    """Central widget, where the picture and everything is drawn."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setMouseTracking(True)  # otherwise no mouseMoveEvents w/o pressing

    # Qt Events.

    def paintEvent(self, event):
        painter = QPainter(self)

        mw = self.parentWidget()  # get main window which has all the info

        # First, paint the image.
        painter.drawPixmap(self.rect(), mw.image)

        # Shortcuts.
        col = Qt.GlobalColor
        def draw_line(p1, p2, opacity, *penargs):
            painter.setOpacity(opacity)
            painter.setPen(QPen(*penargs))
            painter.drawLine(p1, p2)

        # Length (scale) points.
        if mw.points_scale:
            start = self.to_screen_coord(mw.points_scale[0])
            end = (self.to_screen_coord(mw.points_scale[1])
                   if len(mw.points_scale) > 1 else mw.end)
            draw_line(start, end, 1, col.black, 7)
            draw_line(start, end, 1, col.white, 4, Qt.PenStyle.DotLine)

        # Area points.
        aps = mw.points_area  # shortcut ("area points")

        painter.setPen(QPen(QColor(50, 50, 50, 220), 2))  # grayish line
        painter.setBrush(QBrush(QColor(50, 50, 50, 110)))  # light gray fill

        if (mw.area_done or mw.area_mode) and aps:  # fill polygon
            p0 = self.to_screen_coord(aps[0]) if mw.area_done else mw.end
            painter.drawPolygon([self.to_screen_coord(p) for p in aps] + [p0])

        painter.setPen(col.black)  # black border
        painter.setBrush(col.white)  # white fill

        for i, ap in enumerate(aps):  # draw all points (as little squares)
            r = 4 if i == mw.moving else 5  # "radius"
            p = self.to_screen_coord(ap)
            painter.drawRect(p.x()-r, p.y()-r, 2*r, 2*r)

    def mousePressEvent(self, event):
        mw = self.parentWidget()  # get main window which has all the info

        if event.button() == Qt.MouseButton.MiddleButton:  # middle-click
            if mw.area_mode and not mw.area_done and len(mw.points_area) > 2:
                self.set_area()  # middle-click closes the area

        if event.button() != Qt.MouseButton.LeftButton:
            return  # only process left-click from now on

        mw.saved_points = []  # no redo now
        mw.end = event.pos()  # pointer is endpoint for lines that we may draw

        # Scale mode.
        if mw.scale_mode:
            mw.saved = False

            if len(mw.points_scale) == 2:  # we were done, but got a click
                mw.points_scale = []  # so we start afresh

            mw.points_scale.append(self.to_image_coord(mw.end))

            if len(mw.points_scale) == 2:  # done!
                self.set_scale()

        if not mw.area_mode:
            return  # only continue if in area mode

        # Area mode.
        aps = mw.points_area  # shortcut ("area points")
        ctrl = event.modifiers() & Qt.KeyboardModifier.ControlModifier

        if not mw.area_done:  # not done? see if we close area, or add a point
            mw.saved = False

            if len(aps) > 1 and self.get_area_point_at(mw.end) is aps[0]:
                self.set_area()  # done!
            else:
                p = self.to_image_coord(mw.end)
                aps.append(p if not ctrl else self.find_border(p))  # add point
                self.update()
        else:  # we were done? see if we move or insert a point
            p = self.get_area_point_at(mw.end)
            if p:  # we clicked on an existing point -> move it
                mw.saved = False
                mw.moving = aps.index(p)
                self.update()
            elif ctrl:  # we clicked between points, pressing ctrl -> insert one
                end = self.to_image_coord(mw.end)  # pointer in image coords
                dist = lambda p: (end - p).manhattanLength()
                p1, p2 = sorted(aps, key=dist)[:2]  # points closer to pointer
                i1, i2 = sorted([aps.index(p1), aps.index(p2)])  # so i1 < i2
                if i1 == 0 and i2 == len(aps) - 1:  # special case: last points
                    i1, i2 = len(aps) - 1, len(aps)

                if i2 - i1 == 1:  # we clicked between two consecutive points
                    mw.saved = False
                    aps.insert(i2, end)
                    mw.moving = i2
                    self.set_area()

    def mouseReleaseEvent(self, event):
        mw = self.parentWidget()  # get main window which has all the info

        if mw.moving is not None:  # stop moving area point
            mw.moving = None
            self.set_area()

    def mouseMoveEvent(self, event):
        mw = self.parentWidget()  # get main window which has all the info

        if not mw.area_mode and not mw.scale_mode and mw.moving is None:
            return

        if mw.moving is not None:
            mw.points_area[mw.moving] = self.to_image_coord(event.pos())

        points = mw.points_scale if mw.scale_mode else mw.points_area

        if points:
            mw.end = event.pos()
            self.update()

    # Custom functions (not overloaded from Qt).

    def to_image_coord(self, point_in_screen):
        """Return point with coordinates transformed to image pixels."""
        image = self.parentWidget().image
        scale_x = image.width() / self.width()
        scale_y = image.height() / self.height()
        x, y = point_in_screen.x(), point_in_screen.y()

        return QPoint(round(scale_x * x), round(scale_y * y))

    def to_screen_coord(self, point_in_image):
        """Return point with coordinates transformed to screen pixels."""
        image = self.parentWidget().image
        scale_x = image.width() / self.width()
        scale_y = image.height() / self.height()
        x, y = point_in_image.x(), point_in_image.y()

        return QPoint(round(x / scale_x), round(y / scale_y))

    def set_scale(self):
        """Get length between scale points, find scale, store and show it."""
        mw = self.parentWidget()  # get main window which has all the info

        length = mw.get_length()  # ask for the length between the points

        if not length:  # no reply? then reset scale points and be done
            mw.points_scale = []
            self.update()
            return

        # Get value, store it and show it.
        p1, p2 = mw.points_scale
        p12 = p2 - p1
        dist_pixels = (p12.x()**2 + p12.y()**2)**0.5

        info = mw.info[mw.images[mw.current]]
        info['scale'] = dist_pixels / length
        info['points_scale'] = mw.points_scale

        scale, _, _ = get_scale_area(info)
        mw.statusBar().showMessage(f'Scale: {scale} pixel/mm')

    def set_area(self):
        """Mark the selected area as done, store it and show its value."""
        mw = self.parentWidget()  # get main window which has all the info

        mw.area_done = True  # mark as done

        # Get value, store it and show it.
        area = mw.get_area()

        info = mw.info[mw.images[mw.current]]
        info['area'] = area
        info['points_area'] = mw.points_area
        info['area_done'] = True

        _, a_px2, a_cm2 = get_scale_area(info)
        message = (f'Area: {a_px2} pixel²' +
                   (f' ({a_cm2} cm²)' if a_cm2 else ''))
        mw.statusBar().showMessage(message)

        self.update()  # so we see the nicely closed area

    def get_area_point_at(self, point):
        """Return the area-point at the given screen point, or None."""
        # We use the "elevator metric" (inside a square), not manhattanLength.
        is_close = lambda p: (abs(p.x() - point.x()) < 10 and
                              abs(p.y() - point.y()) < 10)

        return next((p for p in self.parentWidget().points_area
                     if is_close(self.to_screen_coord(p))), None)

    def find_border(self, p):
        """Return a point near p, that seems to be on a border."""
        mw = self.parentWidget()  # get main window which has all the info

        if not mw.points_area:
            return p

        v = p - mw.points_area[-1]  # vector from last point to p
        size = 30 * mw.image.width() / self.width() / v.manhattanLength()
        w = size * QPoint(v.y(), -v.x())  # perpendicular to v
        q0 = p - 0.5*w

        steps = w.manhattanLength()
        if steps < 3:
            return p

        im = mw.image.toImage()
        hues = []
        for i in range(steps):
            q = q0 + i/steps * w
            hues.append(im.pixelColor(q).hue())

        def variance(xs, i):
            xs1, xs2 = xs[:i], xs[i:]  # break xs in 2 parts, before and after i
            m1 = sum(xs1) / len(xs1)  # means
            m2 = sum(xs2) / len(xs2)
            return (sum((x - m1)**2 for x in xs1) +
                    sum((x - m2)**2 for x in xs2))

        var_min, i_min = min((variance(hues, i), i) for i in range(1, steps-1))

        return q0 + i_min/steps * w


class MainWindow(QMainWindow):
    """Main application window. Contains the canvas where the action happens."""

    def __init__(self, images):
        super().__init__()

        self.setWindowIcon(QIcon('coraline.png'))

        self.setCentralWidget(Canvas(self))

        self.add_actions_file(self.menuBar().addMenu('File'))
        self.add_actions_edit(self.menuBar().addMenu('Edit'))

        # We could try to add a menu to select images directly, similar to:
        # if len(images) < 50:  # or some reasonable value
        #     menu = self.menuBar().addMenu('Images')
        #     for image in images:
        #         menu.addAction(self.make_action(
        #             os.path.basename(image),
        #             fn=lambda: self.load_image(image)))
        # else:
        #     self.menuBar().addMenu('(too many images to list)')

        assert images, 'empty list of images'
        self.images = images
        self.current = 0  # current image
        self.info = {}  # dictionary of information about the images
        self.saved = True  # all information saved?

        self.load_image()

        self.end = QPoint()  # last mouse position

        self.resize(1024, 768)
        self.show()

    # Qt Events.

    def keyPressEvent(self, event):
        key = event.key()  # shortcuts
        qk = Qt.Key
        ctrl = event.modifiers() & Qt.KeyboardModifier.ControlModifier

        if key == qk.Key_0:                  # 0  original zoom
            self.resize(self.image.width(),
                        self.image.height())
        elif key == qk.Key_Plus:             # +  double size
            self.resize(self.width()  * 2,
                        self.height() * 2)
        elif key == qk.Key_Minus:            # -  half size
            self.resize(self.width()  // 2,
                        self.height() // 2)
        elif key == qk.Key_S and not ctrl:   # S  scale mode
            self.activate_scale()
        elif key == qk.Key_A:                # A  area mode
            self.activate_area()
        elif key == qk.Key_N:                # N  find next point automatically
            self.add_next_area_point()
        elif ctrl and key == qk.Key_Z:  # Ctrl+Z  undo
            self.undo()
        elif ctrl and key == qk.Key_Y:  # Ctrl+Y  redo
            self.redo()
        elif key == qk.Key_Right:       # ->      next image
            self.switch_image(shift=+1)
        elif key == qk.Key_Left:        # <-      previous image
            self.switch_image(shift=-1)
        elif ctrl and key == qk.Key_S:  # Ctrl+S  save
            self.save()
        elif ctrl and key == qk.Key_Q:  # Ctrl+Q  quit
            self.quit()

    def contextMenuEvent(self, e):
        context = QMenu(self)

        self.add_actions_edit(context)
        self.add_actions_file(context)

        context.exec(e.globalPos())

    # Custom functions (not overloaded from Qt).

    def make_action(self, text, pixmap, shortcut, fn):
        """Return an action with the given arguments."""
        icon = self.style().standardIcon(pixmap)
        action = QAction(icon, text, self)
        if shortcut:
            action.setShortcut(shortcut)
        action.triggered.connect(fn)
        return action

    def add_actions_edit(self, menu):
        """Add to the given menu the actions related to an edit menu."""
        def add(*args, **kwargs):  # shortcut
            menu.addAction(self.make_action(*args, **kwargs))

        sp = QStyle.StandardPixmap  # shortcut

        add('Set &scale (reference)', sp.SP_TitleBarMinButton,
            'S', self.activate_scale)
        add('Surround &area', sp.SP_DialogYesButton,
            'A', self.activate_area)
        add('Guess &next area point', sp.SP_MediaPlay,
            'N', self.add_next_area_point)

        add('&Undo', sp.SP_MediaSeekBackward, 'Ctrl+Z', self.undo)
        add('&Redo', sp.SP_MediaSeekForward, 'Ctrl+Y', self.redo)

        add('&Clear all', sp.SP_LineEditClearButton, '', self.clear_all)

    def add_actions_file(self, menu):
        """Add to the given menu the actions related to a file menu."""
        def add(*args, **kwargs):  # shortcut
            menu.addAction(self.make_action(*args, **kwargs))

        sp = QStyle.StandardPixmap  # shortcut

        add('&Next image', sp.SP_ArrowForward,
            'Right', lambda: self.switch_image(+1))
        add('&Previous image', sp.SP_ArrowBack,
            'Left', lambda: self.switch_image(-1))

        add('&Save areas', sp.SP_DialogSaveButton, 'Ctrl+S', self.save)
        add('About', sp.SP_MessageBoxInformation, '', self.about)
        add('&Quit', sp.SP_TabCloseButton, 'Ctrl+Q', self.quit)

    def get_length(self):
        """Ask for length and return it. Insist until we get a valid answer."""
        while True:
            length, ok = QInputDialog.getText(self, 'Measure', 'Length (mm):')
            try:
                return float(length) if ok else None
            except ValueError:
                pass  # no valid answer? keep insisting

    def get_area(self):
        """Return the area surrounded by points, in squared pixels."""
        ps = self.points_area  # shortcut

        area = 0
        for i in range(1, len(ps)-1):
            # Sides of triangle.
            v1 = ps[i]   - ps[0]
            v2 = ps[i+1] - ps[0]

            # Coordinates.
            x1, y1 = v1.x(), v1.y()
            x2, y2 = v2.x(), v2.y()

            # Signed area of triangle.
            area += (x1 * y2 - x2 * y1) / 2

        return abs(area)

    def add_next_area_point(self):
        """Add an area point guessing where it may be."""
        if not self.area_mode or len(self.points_area) < 2:
            message = 'Point can be guessed only in area mode with > 1 points'
            self.statusBar().showMessage(message, 5000)
            return

        p, q = self.points_area[-2:]  # last 2 points
        r = q + (q - p)  # point that would continue the line

        if 0 <= r.x() < self.image.width() and 0 <= r.y() < self.image.height():
            self.points_area.append(self.centralWidget().find_border(r))
            self.update()
        else:
            self.statusBar().showMessage('Point would be out of bounds', 5000)

    def load_image(self):
        """Load current image in the pixmap and initialize related variables."""
        fname = self.images[self.current]

        self.setWindowTitle('Coraline - %s' % fname)

        self.image = QPixmap(fname)

        info = self.info.get(fname, {})

        if 'date' not in info:
            exif = Image.open(fname)._getexif() or {}
            tags = {ExifTags.TAGS.get(k): v for k, v in exif.items()}
            date = tags.get('DateTimeOriginal')
            if date:
                info['date'] = date

        self.info[fname] = info  # initializes info on the image

        # Initialize or restore state for this image.
        self.points_scale = info.get('points_scale', [])  # ends of scale line
        self.points_area = info.get('points_area', [])  # vertices of area
        self.moving = None  # index of area point being moved
        self.saved_points = []  # area points saved for redo ("undo undo")
        self.scale_mode = False  # are we drawing the line to set the scale?
        self.area_mode = False  # are we drawing the area?
        self.area_done = info.get('area_done', False)  # area already defined?

        message = (('Loaded ' + os.path.basename(fname)) +
                   (' (taken on %s)' % info['date'] if 'date' in info else ''))
        self.statusBar().showMessage(message, 5000)

    def switch_image(self, shift=+1):
        """Change the current image to the next in the list of images."""
        if len(self.images) < 2:
            return
        self.current = (self.current + shift) % len(self.images)
        self.load_image()
        self.update()

    def activate_scale(self):
        """Set scale mode and update state variables accordingly."""
        self.statusBar().showMessage(
            'Scale mode - click to set reference length')

        if self.scale_mode:
            return

        self.scale_mode = True

        self.area_mode = False
        self.saved_points = []
        self.update()

    def activate_area(self):
        """Set area mode and update state variables accordingly."""
        self.statusBar().showMessage(
            'Area mode - click to encircle an area')

        if self.area_mode:
            return

        self.area_mode = True

        self.scale_mode = False
        if len(self.points_scale) == 1:
            self.points_scale = []
        self.saved_points = []
        self.update()

    def clear_all(self):
        """Clear all state variables and information saved for current image."""
        self.scale_mode = False
        self.area_mode = False
        self.area_done = False
        self.moving = None
        self.points_scale = []
        self.points_area = []
        self.saved_points = []
        info = self.info[self.images[self.current]]
        for k in ['scale', 'area', 'points_scale', 'points_data', 'area_done']:
            info.pop(k, None)
        self.statusBar().clearMessage()
        self.update()

    def undo(self):
        """Remove the last added point."""
        if not self.area_mode and not self.scale_mode:
            return

        points = self.points_scale if self.scale_mode else self.points_area
        if points:
            if self.area_mode and self.area_done:
                self.area_done = False
            else:
                self.saved_points.append(points.pop())
        self.update()

    def redo(self):
        """Add the last removed point with undo."""
        if (not self.saved_points or
            (not self.area_mode and not self.scale_mode)):
            return

        points = self.points_scale if self.scale_mode else self.points_area
        points.append(self.saved_points.pop())
        self.update()

    def save(self):
        """Save areas and all info from visited images to a csv file."""
        name, _ = QFileDialog.getSaveFileName(self, 'Save File',
                                              'areas.csv', '*.csv')
        if not name:
            return

        with open(name, 'wt') as fout:
            fout.write(
                'Picture,Date,Scale (pixel/mm),Area (pixel²),Area (cm²)\n')
            for fname_unsafe, info in self.info.items():
                fname = fname_unsafe.replace('"', '""')  # escape quotes
                date = info.get('date', '')
                scale, area, area_cm2 = get_scale_area(info)
                fout.write(f'"{fname}","{date}",{scale},{area},{area_cm2}\n')

        self.saved = True
        self.statusBar().showMessage(f'Measures saved in {name}', 5000)

    def about(self):
        box = QMessageBox(QMessageBox.Icon.Information, 'About',
                          'Coraline\n(c) Anna Harvey and team')
        box.setDetailedText(
            'A program to measure the areas of corals by drawing lines.\n\n'
            'For more information, see https://gitlab.com/jordibc/coraline')
        box.exec()

    def quit(self):
        if self.saved:
            self.close()
        else:
            sb = QMessageBox.StandardButton  # shortcut
            if QMessageBox.question(
                    self, 'Quit', 'There may be unsaved changes. Are you sure?',
                    sb.Yes | sb.No, sb.No) == sb.Yes:
                self.close()


def main():
    warnings.simplefilter('ignore')  # avoid silly DeprecationWarning
    signal.signal(signal.SIGINT, signal.SIG_DFL)  # so qt doesn't stop Ctrl+C

    app = QApplication(sys.argv)

    if len(sys.argv) == 1:
        images = get_images()
    elif len(sys.argv) == 2:
        path = sys.argv[1]
        images = get_images(path)
    else:
        sys.exit('usage: %s [<dir>]' % sys.argv[0])

    if images:
        window = MainWindow(images)
        sys.exit(app.exec())


def get_images(path=None):
    """Keep on asking for a directory with images, and return a list of them."""
    while True:
        path = QFileDialog.getExistingDirectory(None, 'Select a folder:',
                                                path or '.',
                                                QFileDialog.Option.ShowDirsOnly)
        if not path:
            return []  # didn't select a folder? -> no images

        images = [f'{path}/{x}' for x in os.listdir(path) if is_image(x)]

        if images:
            return images

        sb = QMessageBox.StandardButton  # shortcut
        if QMessageBox.information(
                None, 'Coraline - No images found',
                f'No images found in {path}. Try a different folder?',
                sb.Yes | sb.No, sb.Yes) == sb.No:
            return []  # don't want to try again? -> no images


def is_image(fname):
    return any(fname.lower().endswith('.' + ext)
               for ext in ['png', 'jpg', 'jpeg'])


def get_scale_area(info):
    """Return strings with the scale and area (in pixel^2 and cm^2)."""
    to_str = lambda x: ('%.4g' % info[x]) if x in info else ''

    scale = to_str('scale')  # in pixel/mm
    area = to_str('area')  # in pixel^2

    area_cm2 = (('%.4g' % (info['area'] / info['scale']**2 / 100))
                if scale and area else '')

    return scale, area, area_cm2



if __name__ == '__main__':
    main()
