# Coraline

A little program to measure the areas of corals, by drawing lines in
pictures that contain some reference scale.


## 📋 Features

The program can quickly move through all the images in a given
directory. For each image, you can:

- Set a reference scale (by pressing `S` to pick two points in the
  image, and say what distance they are apart)
- Set an enclosed area (by pressing `A` to pick the surrounding
  points)
  - `Ctrl-Z / Ctrl-Y` to undo/redo
  - Use the mouse middle click or click on first point to close area
  - `Ctrl-click` to help select the best point, or add an intermediate point
  - Click and drag existing points to adjust them
- Move to the next/previous image (right/left arrow keys)
- Change the zoom level by pressing `+` and `-`
- Save all the computed areas in a csv file (by pressing `Ctrl-S`)

And right-click on the image to get a menu with all the options.


## 📥 Installation

### On Linux / Mac

After downloading and changing to this directory, you can run `pip
install -e .` the first time to install the dependencies, and then
just:

```sh
coraline
```


### On Windows

First, open a console, type `python` and press enter. If you don't
have python installed, it will direct you on how to install it. If you
entered into the python interpreter, just type `exit()` and press
enter to go back to the console.

From the console, run this command too: `pip install pillow pyqt6`. It
will install the necessary libraries/modules to run coraline.

Afterwards, you can download this directory and just double-click on
the `coraline.py` program to run it. It will open a dialog where you
can choose a directory with images, and afterwards will open one and
you can start measuring them.


## ⚖️ License

This program is licensed under the GPL v3. See the [project
license](license.md) for further details.
